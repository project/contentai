<?php

namespace Drupal\Tests\contentai\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\contentai\Services\ContentAIService;

/**
 * Tests the Content AI Service.
 *
 * @group contentai
 */
class ContentAIServiceTest extends UnitTestCase {

  /**
   * Comment manager mock.
   *
   * @var \Drupal\contentai\Services\ContentAIService|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $client;

  public function processContentTest() {
    $openai_service = $this->getMockBuilder('OpenAI\Client')
      ->disableOriginalConstructor()
      ->getMock();
    $openai_service->expects($this->any())
      ->method('completions')
      ->willReturn($openai_service);
    $openai_service->expects($this->any())
      ->method('create')
      ->willReturn($openai_service);
    $openai_service->expects($this->any())
      ->method('toArray')
      ->willReturn([]);
    $this->client = new ContentAIService($openai_service);

    $response = $this->client->processContent('test');
    $this->assertEquals('No terms could be generated from the provided input.', $response);
  }
}